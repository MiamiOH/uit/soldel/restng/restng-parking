<?php


class parkingv1ReadTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $parking;
    private $subClass;
    private $dbh;
    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {
        //Clearing Private Variables

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];
        //Setting up Mock DB Handle

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->subClass = $this->getMockBuilder('\MiamiOH\RestngParking\Services\NuparkSubClassification')
            ->setMethods(array('setup', 'getSubClass'))
            ->getMock();

        //Setting up the Parking Variable

        $this->parking = new \MiamiOH\RestngParking\Services\Parkingv1();

        //Telling Parking variable to use DB Handle

        $this->parking->setDatabase($db);
        $this->parking->setNuparkSubClassification($this->subClass);
    }


    public function testParkingSingle()
    {
        $pidm = '12345';

        $this->records = [
            [   'goradid_additional_id' => '12341234123412',
                'szbuniq_unique_id' => 'test123',
                'szbuniq_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $models = $this->parking->read($pidm);


        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from szbuniq') !== false, 'Query contains from szbuniq');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(1, count($models));

        $record = $this->records[0];
        $model = $models[0];

        $this->assertEquals($record['szbuniq_pidm'], $model['pidm']);
        $this->assertEquals($record['szbuniq_unique_id'], $model['uniqueId']);
        $this->assertEquals($record['goradid_additional_id'], $model['aptiqSerialNumberHex']);
        $this->assertEquals(hexdec($record['goradid_additional_id']), $model['aptiqSerialNumberDec']);
    }
    public function testParkingMulti()
    {
        $pidm = ['12345', '67890', '3456'];

        $this->records = [
            [   'goradid_additional_id' => '12341234123412',
                'szbuniq_unique_id' => 'test123',
                'szbuniq_pidm' => '12345',
            ],
            [   'goradid_additional_id' => '5654567546456',
                'szbuniq_unique_id' => 'test678',
                'szbuniq_pidm' => '67890',
            ],
            [   'goradid_additional_id' => 'ABCDEF',
                'szbuniq_unique_id' => 'test345',
                'szbuniq_pidm' => '3456',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $models = $this->parking->read($pidm);

        $this->assertTrue(in_array($pidm[0], $this->queryParams));
        $this->assertTrue(in_array($pidm[1], $this->queryParams));
        $this->assertTrue(in_array($pidm[2], $this->queryParams));

        $this->assertTrue(strpos($this->queryString, 'from szbuniq') !== false, 'Query contains from szbuniq');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(count($this->records), count($models));


        for ($i=0;$i<count($this->records); $i++) {
            $record = $this->records[$i];
            $this->assertEquals($record['szbuniq_pidm'], $models[$i]['pidm']);
            $this->assertEquals($record['szbuniq_unique_id'], $models[$i]['uniqueId']);
            $this->assertEquals($record['goradid_additional_id'], $models[$i]['aptiqSerialNumberHex']);
            $this->assertEquals(hexdec($record['goradid_additional_id']), $models[$i]['aptiqSerialNumberDec']);
        }
    }

    public function testParkingMultiMissingPerson()
    {
        $pidm = ['12345', '67890', '3456'];

        $this->records = [
            [   'goradid_additional_id' => '12341234123412',
                'szbuniq_unique_id' => 'test123',
                'szbuniq_pidm' => '12345',
            ],
            [   'goradid_additional_id' => '5654567546456',
                'szbuniq_unique_id' => 'test678',
                'szbuniq_pidm' => '67890',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->parking->read($pidm);

        $this->assertTrue(in_array($pidm[0], $this->queryParams));
        $this->assertTrue(in_array($pidm[1], $this->queryParams));
        $this->assertTrue(in_array($pidm[2], $this->queryParams));

        $this->assertTrue(strpos($this->queryString, 'from szbuniq') !== false, 'Query contains from szbuniq');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(count($this->records), count($models));


        for ($i=0;$i<count($this->records); $i++) {
            $record = $this->records[$i];
            if ($record['szbuniq_pidm'] != '3456' && $models[$i]['pidm'] != '3456') {
                $this->assertEquals($record['szbuniq_pidm'], $models[$i]['pidm']);
                $this->assertEquals($record['szbuniq_unique_id'], $models[$i]['uniqueId']);
                $this->assertEquals($record['goradid_additional_id'], $models[$i]['aptiqSerialNumberHex']);
                $this->assertEquals(hexdec($record['goradid_additional_id']), $models[$i]['aptiqSerialNumberDec']);
            } else {
                $this->fail('PIDM 3456 was Returned');
            }
        }
    }

    public function testParkingInvalidId()
    {
        $pidm = ';dsf;l';

        $this->records = [
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $models = $this->parking->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from goradid') !== false, 'Query contains from goradid');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(0, count($models));
    }

    public function testParkingNoIds()
    {
        $pidm = '';

        $this->records = [
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $models = $this->parking->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from goradid') !== false, 'Query contains from goradid');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(0, count($models));
    }

    public function testParkingNoAqpiqNumber()
    {
        $pidm = '12345';

        $this->records = [
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $models = $this->parking->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from goradid') !== false, 'Query contains from goradid');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');


        $this->assertEquals(0, count($models));
    }
    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString=$subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
