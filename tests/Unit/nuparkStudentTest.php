<?php

/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/23/18
 * Time: 9:09 AM
 */
class nuparkStudentTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $student;
    private $dbh;
    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {
        //Clearing Private Variables

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];
        //Setting up Mock DB Handle

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //Setting up the Parking Variable

        $this->student = new \MiamiOH\RestngParking\Services\NuparkStudent();

        //Telling Parking variable to use DB Handle

        $this->student->setDatabase($db);
    }


    public function testIsRegionalStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => '',
                'sgrsatt_atts_code' => '',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '19',
                'classcode' => '',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isRegionalStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testIsPathWayStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => 'NPTH',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => '',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '19',
                'classcode' => '',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isPathWayStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testisHeritageCommonStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => 'GR',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'HC',
                'zipcode' => '45056',
                'age' => '30',
                'classcode' => '99',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isHeritageCommonsResident($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testisNonTraditionalStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => '',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '30',
                'classcode' => '',
                'fallspringenrolled' => '',
                'distance' => 'N',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isNonTraditionalStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }




    public function testisDoctoralStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgbstdn_rate_code' => '',
                'sgrsatt_atts_code' => '',
                'sgbstdn_levl_code' => '',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '30',
                'classcode' => '15',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isDoctoralStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testisNonDegreeStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgbstdn_rate_code' => '',
                'sgrsatt_atts_code' => '',
                'sgbstdn_levl_code' => '',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '30',
                'classcode' => '99',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isNonDegreeStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }


    public function testisGradStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgbstdn_rate_code' => '',
                'sgrsatt_atts_code' => '',
                'sgbstdn_levl_code' => 'GR',
                'sgbstdn_camp_code' => 'M',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'N',
                'slrrasg_bldg_code' => 'MOR',
                'zipcode' => '45056',
                'age' => '30',
                'classcode' => '99',
                'fallspringenrolled' => '',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isGradStudent($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }




    public function testIsSophomoreStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgbstdn_rate_code' => '',
                'sgrsatt_atts_code' => '',
                'sgbstdn_levl_code' => 'UG',
                'sgbstdn_camp_code' => 'O',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'J',
                'slrrasg_bldg_code' => 'HLH',
                'zipcode' => '43109',
                'age' => '20',
                'classcode' => '02',
                'fallspringenrolled' => 'N',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isSophomore($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }


    public function testIsFirstYearStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => 'UG',
                'sgbstdn_camp_code' => 'O',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'J',
                'slrrasg_bldg_code' => 'HLH',
                'zipcode' => '43109',
                'age' => '20',
                'classcode' => '01',
                'fallspringenrolled' => 'N',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));


        $this->student->setup($pidm);
        $this->assertTrue($this->student->isFirstYear($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }


    public function testIsFirstYearStudentDistance()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => 'UG',
                'sgbstdn_camp_code' => 'O',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'J',
                'slrrasg_bldg_code' => 'HLH',
                'zipcode' => '43109',
                'age' => '20',
                'classcode' => '01',
                'fallspringenrolled' => 'N',
                'distance' => 'Y',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));


        $this->student->setup($pidm);
        $this->assertTrue($this->student->isFirstYearStudentDistance($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testJuniorStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => 'UG',
                'sgbstdn_camp_code' => 'O',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'J',
                'slrrasg_bldg_code' => 'HLH',
                'zipcode' => '43109',
                'age' => '20',
                'classcode' => '03',
                'fallspringenrolled' => 'N',
                'distance' => 'N',

            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $this->student->setup($pidm);
        $this->assertTrue($this->student->isJunior($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }



    public function testSeniorStudent()
    {
        $pidm = '12345';

        $this->records = [
            [   'sgbstdn_pidm' => '12345',
                'sgrsatt_atts_code' => '',
                'sgbstdn_rate_code' => '',
                'sgbstdn_levl_code' => 'UG',
                'sgbstdn_camp_code' => 'O',
                'sgbstdn_term_code_eff' => '201810',
                'sgbstdn_styp_code' => 'J',
                'slrrasg_bldg_code' => null,
                'zipcode' => '43109',
                'age' => '20',
                'classcode' => '04',
                'fallspringenrolled' => 'N',
                'distance' => 'N',


            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setup($pidm);
        $this->assertTrue($this->student->isSenior($pidm));
        $this->assertFalse($this->student->isJunior($pidm));
        $this->assertFalse($this->student->isSophomore($pidm));
        $this->assertFalse($this->student->isFirstYear($pidm));
        $this->assertFalse($this->student->isOffCampusStudent($pidm));
        $this->assertTrue($this->student->isCommuterStudent($pidm));
        $this->assertFalse($this->student->isEnrolledFallSpring($pidm));


        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from sgbstdn') !== false, 'Query contains from sgbstdn');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }


    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString=$subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
