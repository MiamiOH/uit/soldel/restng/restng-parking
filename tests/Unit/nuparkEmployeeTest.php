<?php

/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 10:12 AM
 */
class nuparkEmployeeTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $employee;
    private $dbh;
    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {
        //Clearing Private Variables

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];
        //Setting up Mock DB Handle

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //Setting up the Parking Variable

        $this->employee = new \MiamiOH\RestngParking\Services\NuparkEmployee();

        //Telling Parking variable to use DB Handle

        $this->employee->setDatabase($db);
    }

    public function testHighSchoolEmployeeSingle()
    {
        $pidm = '12345';

        $this->records = [
            [   'pebempl_ecls_code' => 'HS',
                'pebempl_bcat_code' => 'asdfasdfasdf',
                'pebempl_egrp_code' => 'asfasdfasdf',
                'pebempl_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->employee->setup($pidm);
        $this->assertTrue($this->employee->isEmployee($pidm));
        $this->assertTrue($this->employee->isHighSchool($pidm));

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from pebempl') !== false, 'Query contains from pebempl');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }

    public function testPayrollDeductEmployee()
    {
        $pidm = '12345';

        $this->records = [
            [   'pebempl_ecls_code' => 'HSadsfasdf',
                'pebempl_bcat_code' => 'asdfasdfasdf',
                'pebempl_egrp_code' => 'asfasdfasdf',
                'pebempl_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $this->employee->setup($pidm);
        $this->assertTrue($this->employee->isEmployee($pidm));
        $this->assertFalse($this->employee->isHighSchool($pidm));
        $this->assertTrue($this->employee->isPayrollDeduct($pidm));

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from pebempl') !== false, 'Query contains from pebempl');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }

    public function testNonPayrollDeductEmployee()
    {
        $pidms = ['12345', '3456','6787'];

        $this->records = [
            [   'pebempl_ecls_code' => 'NP',
                'pebempl_bcat_code' => 'asdfasdfasdf',
                'pebempl_egrp_code' => 'asfasdfasdf',
                'pebempl_pidm' => '12345',
            ],
            [   'pebempl_ecls_code' => 'SC',
                'pebempl_bcat_code' => 'asdfasdfasdf',
                'pebempl_egrp_code' => 'asfasdfasdf',
                'pebempl_pidm' => '3456',
            ],
            [   'pebempl_ecls_code' => 'HSadsfasdf',
                'pebempl_bcat_code' => 'asdfasdfasdf',
                'pebempl_egrp_code' => 'ERET',
                'pebempl_pidm' => '6787',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        // $this->dbh->queryall_array("select * from bleh", ['bleh', 'bkea']);
        $this->employee->setup($pidms);
        foreach ($pidms as $pidm) {
            $this->assertTrue($this->employee->isEmployee($pidm));
            $this->assertFalse($this->employee->isHighSchool($pidm));
            $this->assertFalse($this->employee->isPayrollDeduct($pidm));
        }

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'from pebempl') !== false, 'Query contains from pebempl');
        $this->assertTrue(strpos($this->queryString, 'where') !== false, 'Query contains Where');
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString=$subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
