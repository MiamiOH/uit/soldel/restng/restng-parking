<?php


use MiamiOH\RESTng\App;

class parkingv1RestGetTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $parkingv1REST;
    private $parking;
    private $request;
    private $parkingData = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    private $mockReadResponse = [];
    private $readPidm = '';
    private $mockApp;


    protected function setUp(): void
    {
        $this->parkingData = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];
        $this->mockReadResponse = [];
        $this->readPidm = '';


        //set up the mock api:
        $this->mockApp = $this->createMock(App::class);

        $this->mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->parking = $this->getMockBuilder('\MiamiOH\RestngParking\Services\Parkingv1')
            ->setMethods(array('read'))
            ->getMock();

        $this->parking->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));


        //set up the service with the mocked out resources:
        $this->parkingv1REST = new \MiamiOH\RestngParking\Services\Parkingv1REST();

        $this->parkingv1REST->setApp($this->mockApp);
        $this->parkingv1REST->setLogger();
        $this->parkingv1REST->setBannerUtil($bannerUtil);
        $this->parkingv1REST->setparkingv1($this->parking);
    }


    public function testGetParkingREST()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'nuparkSubClassificationTest' => 'V',
            'aptiqSerialNumber' => '12345',

        ];

        $this->parkingv1REST->setRequest($this->request);

        $response = $this->parkingv1REST->getParkingv1();

        $payload = $response->getPayload();

        $this->assertNotEquals([$this->mockReadResponse], $payload);
    }


    public function testGetParkingv1RESTInvalidId()
    {
        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = DOEJ'")));

        $this->parkingv1REST->setRequest($this->request);

        $response = $this->parkingv1REST->getParkingv1();

        $this->assertEquals(MiamiOH\RESTng\App::API_NOTFOUND, $response->getStatus());
    }


    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;
        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }

    public function readWith($subject)
    {
        $this->readPidm = $subject;
        return true;
    }

    public function readMock()
    {
        return $this->mockReadResponse;
    }
}
