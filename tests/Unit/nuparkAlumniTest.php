<?php

/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 10:12 AM
 */


class nuparkAlumniTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $emeriti;
    private $dbh;
    private $records = [];
    private $queryString = '';
    private $queryParams = [];


    protected function setUp(): void
    {
        //Clearing Private Variables

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];
        //Setting up Mock DB Handle

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //Setting up the Parking Variable

        $this->emeriti = new \MiamiOH\RestngParking\Services\NuparkAlumini();

        //Telling Parking variable to use DB Handle

        $this->emeriti->setDatabase($db);
    }


    public function testIsEmeriti()
    {
        $pidm = '12345';

        $this->records = [
            [   'aprpros_prcd_code' => 'SOME' || 'SW' || 'SSP',
                'aprpros_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->emeriti->setup($pidm);
        $this->assertTrue($this->emeriti->isEmeriti($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'FROM apbcons, aprpros') !== false, 'Query contains FROM aprpros,apbcons');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false, 'Query contains Where');
    }




    public function testIsRetire()
    {
        $pidm = '12345';

        $this->records = [
            [   'aprpros_prcd_code' => 'SOMR' || 'RSW'|| 'RSSP',
                'aprpros_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->emeriti->setup($pidm);
        $this->assertTrue($this->emeriti->isRetiree($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'FROM apbcons, aprpros') !== false, 'Query contains FROM aprpros,apbcons');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false, 'Query contains Where');
    }



    public function testIsAlumni()
    {
        $pidm = '12345';

        $this->records = [
            [   'aprpros_prcd_code' => 'SOMR',
                'aprpros_pidm' => '12345',
            ],
        ];


        $this->dbh->expects($this->once())->method('queryall_array')
            ->with(
                $this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams'))
            )
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->emeriti->setup($pidm);
        $this->assertTrue($this->emeriti->isAlumni($pidm));
        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(strpos($this->queryString, 'FROM apbcons, aprpros') !== false, 'Query contains FROM aprpros,apbcons');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false, 'Query contains Where');
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString=$subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
