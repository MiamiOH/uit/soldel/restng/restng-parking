<?php

/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 10:12 AM
 */
class nuparkSubClassificationTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $subclassification;
    private $employeeInfo;
    private $studentInfo;
    private $emeritiInfo;
    private $records = [];


    protected function setUp(): void
    {
        //Clearing Private Variables

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];
        //Setting up Mock DB Handle

        $this->employeeInfo = $this->getMockBuilder('\MiamiOH\RestngParking\Services\NuparkEmployee')
            ->setMethods(array('setup', 'isHighSchool', 'isEmployee', 'isPayrollDeduct','isNonMiamiCollegeStudentEmp','isGradAssistant'))
            ->getMock();

        $this->studentInfo = $this->getMockBuilder('\MiamiOH\RestngParking\Services\NuparkStudent')
            ->setMethods(array('setup', 'isEnrolledFallSpring','isRegionalStudent', 'isNonTraditionalStudent',
                'isGradAssistant','isNonDegreeStudent','isDoctoralStudent','isGradStudent','isPostSecondaryStudent'
            ,'isSenior','isHeritageCommonsResident','isResidentHallStudent','isOffCampusStudent','isCommuterStudent'
            ,'isJunior','isSophomore','isFirstYear','isFirstYearStudentDistance','isPathWayStudent'))
            ->getMock();
        $this->emeritiInfo = $this->getMockBuilder('\MiamiOH\RestngParking\Services\NuparkAlumini')
            ->setMethods(array('setup','isEmeriti','isRetiree','isAlumni'))
            ->getMock();


        //Setting up the Parking Variable

        $this->subclassification = new \MiamiOH\RestngParking\Services\NuparkSubClassification();
        $this->subclassification->setNuparkEmployee($this->employeeInfo);
        $this->subclassification->setNuparkStudent($this->studentInfo);
        $this->subclassification->setNuparkAlumini($this->emeritiInfo);



        //Telling Parking variable to use DB Handle
    }


    public function testHighSchoolEmployeeSingle()
    {
        $pidm = '12345';


        $this->employeeInfo->expects($this->once())->method('isEmployee')
            ->will($this->returnValue(true));
        $this->employeeInfo->expects($this->once())->method('isHighSchool')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'HSE');
    }




    public function testPayrollDeduct()
    {
        $pidm = '12345';


        $this->employeeInfo->expects($this->once())->method('isEmployee')
            ->will($this->returnValue(true));
        $this->employeeInfo->expects($this->once())->method('isHighSchool')
            ->will($this->returnValue(false));
        $this->employeeInfo->expects($this->once())->method('isNonMiamiCollegeStudentEmp')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isGradAssistant')
            ->will($this->returnValue(false));
        $this->employeeInfo->expects($this->once())->method('isPayrollDeduct')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'EMP');
    }

    public function testNotPayrollDeduct()
    {
        $pidm = '12345';


        $this->employeeInfo->expects($this->once())->method('isEmployee')
            ->will($this->returnValue(true));
        $this->employeeInfo->expects($this->once())->method('isHighSchool')
            ->will($this->returnValue(false));
        $this->employeeInfo->expects($this->once())->method('isNonMiamiCollegeStudentEmp')
            ->will($this->returnValue(false));
        $this->employeeInfo->expects($this->once())->method('isPayrollDeduct')
            ->will($this->returnValue(false));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'EMP-PIO');
    }


    public function testRegionalStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isRegionalStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'REG');
    }


    public function testisNonDegreeStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isRegionalStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isNonDegreeStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'NDS');
    }

    public function testisDoctoralStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isRegionalStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isNonDegreeStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isDoctoralStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'DOC');
    }


    public function testisGradStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isRegionalStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isNonDegreeStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isDoctoralStudent')
            ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isGradStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'GS');
    }



    public function testisPostSecondaryStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isPostSecondaryStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'POST');
    }


    public function testisNonTraditionalStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
        ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isPostSecondaryStudent')
        ->will($this->returnValue(false));
        $this->studentInfo->expects($this->once())->method('isNonTraditionalStudent')
        ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'NON');
    }


    public function testisSenior()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSenior')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SR-OFF');   // Updated default from SR to SR-OFF
    }


    public function testisSeniorHeritageCommonsResident()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSenior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isHeritageCommonsResident')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SR-HC');
    }



    public function testisSeniorResidentHallStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSenior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isResidentHallStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SR-RH');
    }


    public function testisSeniorOffCampusStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSenior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isOffCampusStudent')
         ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SR-OFF');
    }



    public function testisSeniorCommuterStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSenior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isCommuterStudent')
         ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SR-OFF');   // Updated default from SR-C to SR-OFF
    }


    public function testisJunior()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isJunior')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'JR-OFF');   // Updated default from JR to JR-OFF
    }


    public function testisJuniorCommuter()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isJunior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isCommuterStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'JR-OFF');   // Updated from JR-C to JR-OFF
    }



    public function testisJuniorHeritageCommonsResident()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isJunior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isHeritageCommonsResident')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'JR-HC');
    }





    public function testisJuniorResidentHallStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isJunior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isResidentHallStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'JR-RH');
    }


    public function testisJuniorOffCampusStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isJunior')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isOffCampusStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'JR-OFF');
    }



    public function testisSophomore()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isSophomore')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SOF-RH');    // Updated default from SOF to SOF-RH
    }


    public function testisSophomoreCommuter()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isSophomore')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isCommuterStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SOF-RH');   // Originally SOF-C. Updated to SOF-RH
    }



    public function testisSophomoreHeritageCommonsResident()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isSophomore')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isHeritageCommonsResident')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SOF-HC');
    }





    public function testisSophomoreOffCampusStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isSophomore')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isOffCampusStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'SOF-OFF');
    }


    public function testisFirstYear()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isFirstYear')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'FY');
    }



    public function testisFirstYearCommuter()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isFirstYear')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isCommuterStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'FY-C');
    }


    public function testisFirstYearDistance()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));

        $this->studentInfo->expects($this->once())->method('isFirstYear')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isFirstYearStudentDistance')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'FY-D');
    }


    public function testVistor()
    {
        $pidm = '12345';
        ;

        $this->employeeInfo->expects($this->once())->method('isEmployee')
            ->will($this->returnValue(false));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'V');
    }

    public function testisEmeriti()
    {
        $pidm = '12345';

        $this->emeritiInfo->expects($this->once())->method('isEmeriti')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'EMER');
    }

    public function testisRetire()
    {
        $pidm = '12345';

        $this->emeritiInfo->expects($this->once())->method('isRetiree')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'EMER');
    }

    public function testisAlumini()
    {
        $pidm = '12345';

        $this->emeritiInfo->expects($this->once())->method('isAlumni')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'EMER');
    }


    public function testIsPathWayStudent()
    {
        $pidm = '12345';

        $this->studentInfo->expects($this->once())->method('isEnrolledFallSpring')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isRegionalStudent')
            ->will($this->returnValue(true));
        $this->studentInfo->expects($this->once())->method('isPathWayStudent')
            ->will($this->returnValue(true));
        $subClass = $this->subclassification->getSubClass($pidm);
        $this->assertTrue($subClass == 'NPTH');
    }
}
