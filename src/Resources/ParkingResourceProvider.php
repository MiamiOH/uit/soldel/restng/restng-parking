<?php

namespace MiamiOH\RestngParking\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ParkingResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(


            'name' => 'Person.Parking.v1',
            'type' => 'object',
            'properties' => array(

                'pidm' => array(
                    'type' => 'number',
                ),
                'uniqueId' => array(
                    'type' => 'string'
                ),
                'nuparkSubClassification' => array(
                    'type' => 'string',

                ),
                'aptiqSerialNumberHex' => array(
                    'type' => 'string',

                ),
                'aptiqSerialNumberDec' => array(
                    'type' => 'string',

                ),

            ),

        ));

        $this->addDefinition(array(


            'name' => 'Person.Parking.v1.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Parking.v1'
            )

        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
                'name' => 'NuparkEmployee',
                'class' => 'MiamiOH\RestngParking\Services\NuparkEmployee',
                'description' => 'Provide parking information for given unique Id',
                'set' => array(
                    'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
                )
            )
        );

        $this->addService(array(
                'name' => 'NuparkStudent',
                'class' => 'MiamiOH\RestngParking\Services\NuparkStudent',
                'description' => 'Provide parking information for given unique Id',
                'set' => array(
                    'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
                )
            )
        );

        $this->addService(array(
                'name' => 'NuparkAlumini',
                'class' => 'MiamiOH\RestngParking\Services\NuparkAlumini',
                'description' => 'Provide parking information for given unique Id',
                'set' => array(
                    'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory')
                )
            )
        );

        $this->addService(array(
                'name' => 'nuparkSubClassification',
                'class' => 'MiamiOH\RestngParking\Services\NuparkSubClassification',
                'description' => 'Provides Subclassifcation Information',
                'set' => array(
                    'nuparkemployee' => array('type' => 'service', 'name' => 'NuparkEmployee'),
                    'nuparkstudent' => array('type' => 'service', 'name' => 'NuparkStudent'),
                    'nuparkalumini' => array('type' => 'service', 'name' => 'NuparkAlumini')

                )
            )
        );

        $this->addService(array(
                'name' => 'Parkingv1',
                'class' => 'MiamiOH\RestngParking\Services\Parkingv1',
                'description' => 'Provide parking information for given unique Id',
                'set' => array(
                    'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                    'nuparkSubClassification' => array('type' => 'service', 'name' => 'nuparkSubClassification')
                )
            )
        );

        $this->addService(array(
                'name' => 'Parkingv1REST',
                'class' => 'MiamiOH\RestngParking\Services\Parkingv1REST',
                'description' => 'Provide parking information for given unique Id',
                'set' => array(
                    'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                    'parkingv1' => array('type' => 'service', 'name' => 'Parkingv1')

                )
            )
        );

    }

    public function registerResources(): void
    {
        $this->addResource(array(


                'action' => 'read',
                'name' => 'person.parking.v1.muid.get',
                'description' => "##### Description:\nThis resource provides parking information. We are getting aptiqserialnumber and nupark subclassification data based on the muid or pidms.
                      \n##### Authentication:\nConsumer must provide a valid Miami WS token
                      \n##### Authorization:
                      \n- With admin access, consumers are able to get the parking information.
                      \n- Add UniqueID with key `admin` into AuthMan (application: `WebServices`, module: `Person-Parking`).
                      \n- Consumers are ONLY able to get the aptiqserialnumber if it exists. It returns `null` otherwise. And it should return Nupark subclassification data for everyone unless you are a visitor.
                      ",
                'summary' => 'Getting parking information',
                'pattern' => '/person/parking/v1/:muid',
                'service' => 'Parkingv1REST',
                'method' => 'getParkingv1',
                'tags' => array('Person'),
                'returnType' => 'model',
                'params' => array(
                    'muid' => array('description' => 'A Miami identifier', 'alternateKeys' => ['uniqueId', 'pidm']),
                ),

                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        'application' => 'WebServices',
                        'module' => 'Person-Parking',
                        'key' => 'view'
                    )
                ),

                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A person’s parking information.',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/Person.Parking.v1',
                        )
                    )
                ),
            )
        );

        $this->addResource(array(


                'action' => 'read',
                'name' => 'person.parking.v1.get',
                'description' => "##### Description:\nThis resource provides parking information. We are getting aptiqserialnumber and nupark subclassification data based on the pidms.
                      \n##### Authentication:\nConsumer must provide a valid Miami WS token
                      \n##### Authorization:
                      \n- With admin access, consumers are able to get the parking information.
                      \n- Add UniqueID with key `admin` into AuthMan (application: `WebServices`, module: `Person-Parking`).
                      \n- Consumers are ONLY able to get the aptiqserialnumber if it exists. It returns `null` otherwise. And it should return Nupark subclassification data for everyone unless you are a visitor.
                      ",
                'summary' => 'Getting parking information',
                'pattern' => '/person/parking/v1',
                'service' => 'Parkingv1REST',
                'method' => 'getParkingv1Collection',
                'tags' => array('Person'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array(
                        'required' => true,
                        'type' => 'list',
                        'description' => 'A comma separated list of PIDMS to get Parking Information.',
                    )
                ),

                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                    'authorize' => array(
                        'application' => 'WebServices',
                        'module' => 'Person-Parking',
                        'key' => 'view'
                    )
                ),

                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A person’s parking information.',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Person.Parking.v1.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}