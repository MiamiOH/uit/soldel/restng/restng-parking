<?php
/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/24/18
 * Time: 11:05 AM
 */

namespace MiamiOH\RestngParking\Services;


class NuparkAlumini extends \MiamiOH\RESTng\Service
{


    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    private $emeritiInformation = [];


    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }


    public function setup($pidms)
    {
        if (is_scalar($pidms)) {
            $pidms = array($pidms);
        }
        $values = $pidms;
        $queryString = 'SELECT aprpros_pidm, aprpros_prcd_code
           FROM apbcons, aprpros
          WHERE     aprpros_pidm = apbcons_pidm
           AND aprpros_prcd_code IN (\'SOME\', \'SW\', \'SSP\',\'SOMR\',\'RSW\',\'RSSP\')
                and aprpros_pidm 
                        in (' . implode(',', array_fill(0, count($values), '?')) . ")";


        $records = $this->dbh->queryall_array($queryString, $values);

        for ($i = 0; $i < count($records); $i++) {
            $this->createEmeritiRecord($records[$i]);

        }

    }


    public function createEmeritiRecord($record)
    {

        $model = [];
        $model['pidm'] = $record['aprpros_pidm'];
        $model['prcd_code'] = $record['aprpros_prcd_code'];
        $this->emeritiInformation[$record['aprpros_pidm']] = $model;

    }

    public function isEmeriti($pidm)
    {
// aprpros_prcd_code IN ('SOME', 'SW', 'SSP')
        $prcdcode = $this->emeritiInformation[$pidm]['prcd_code'] ?? null;

        if (isset($this->emeritiInformation[$pidm])) {
            if ($prcdcode == 'SOME' || $prcdcode == 'SW' || $prcdcode == 'SSP') {
                return true;
            }
        }

        return false;

    }


    public function isRetiree($pidm)
    {
        if (isset($this->emeritiInformation[$pidm])) {
            $prcdCode = $this->emeritiInformation[$pidm]['prcd_code'] ?? null;
            if ($prcdCode == 'SOMR' || $prcdCode == 'RSW' || $prcdCode == 'RSSP') {
                return true;
            }
        }

        return false;

    }


    public function isAlumni($pidm)
    {
        if (isset($this->emeritiInformation[$pidm])) {
            if ($this->emeritiInformation[$pidm]['prcd_code'] == 'SOMR') {
                return true;
            }
        }

        return false;

    }


}