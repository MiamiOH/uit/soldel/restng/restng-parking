<?php

namespace MiamiOH\RestngParking\Services;

class Parkingv1REST extends \MiamiOH\RESTng\Service
{


    private $parking;


    public function setparkingv1($parking){

        $this->parking=$parking;

    }


    public function setBannerUtil($bannerUtil){

        $this->bannerUtil=$bannerUtil;


    }


    public function getParkingv1(){

        $this->log->debug('Start the parkingv1 Service');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        try {
            $bannerId=$this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));
            $pidm=$bannerId->getPidm();


            $payload = $this->parking->read($pidm);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload[0]);

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }catch (\Exception $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        return $response;

}

    public function getParkingv1Collection()
    {

        $this->log->debug('Start the parkingv1 Service');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();


        if(!isset($options['pidm'])){
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        try {

            $payload = $this->parking->read($options['pidm']);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        }
        catch (\Exception $e) {
            $this->log->info($e->getMessage());
            $response->setPayload(['error' => $e->getMessage()]);

            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        return $response;


    }


}