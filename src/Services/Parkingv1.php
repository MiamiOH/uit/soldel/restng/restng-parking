<?php

namespace MiamiOH\RestngParking\Services;

class Parkingv1 extends \MiamiOH\RESTng\Service
{

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    private $subClass;



    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    public function setNuparkSubClassification($subClass){
        $this->subClass = $subClass;
    }


    public function read($pidms){

        if(is_scalar($pidms)){
            $pidms=array($pidms);
        }
        $values = $pidms;


        $queryString='select GORADID_ADDITIONAL_ID, szbuniq.SZBUNIQ_UNIQUE_ID, szbuniq.szbuniq_pidm
                        from szbuniq full outer join (select * from goradid where  goradid_adid_code = \'APIQ\')
                        on goradid_pidm = szbuniq_pidm
                        where  NOT REGEXP_LIKE(SZBUNIQ_UNIQUE_ID, \'^RTR[0-9]+$\') and szbuniq_pidm
                        in ('
            .implode(',', array_fill(0, count($values), '?')).')';

        $records = $this->dbh->queryall_array($queryString, $values);
        $this->subClass->setup($pidms);
        for($i=0;$i<count($records);$i++){
            $records[$i]= $this->makeModelFromRecord($records[$i]);

        }

        return $records;
    }


    public function makeModelFromRecord($record)
    {

        $aptiqDecNumber=hexdec($record['goradid_additional_id']."");

        $model = [];
        $model['pidm'] = $record['szbuniq_pidm'];
        $model['uniqueId'] = $record['szbuniq_unique_id'];
        $model['nuparkSubClassification'] = $this->subClass->getSubClass($record['szbuniq_pidm']);
        $model['aptiqSerialNumberHex'] = $record['goradid_additional_id'];
        $model['aptiqSerialNumberDec']= $aptiqDecNumber;


        if($aptiqDecNumber == 0){
            $model['aptiqSerialNumberDec']= "";
        }

        return $model;

    }



}
