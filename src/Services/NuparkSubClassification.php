<?php
/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 9:16 AM
 */

namespace MiamiOH\RestngParking\Services;


class NuparkSubClassification extends \MiamiOH\RESTng\Service
{
    private $employeeInfo;

    private $studentInfo;
    private $emeritiInfo;



    public function setNuparkEmployee($employeeInfo){
        $this->employeeInfo=$employeeInfo;
    }

    public function setNuparkStudent($studentInfo){
        $this->studentInfo=$studentInfo;
    }


    public function setNuparkAlumini($emeritiInfo){
        $this->emeritiInfo=$emeritiInfo;
    }

    public function setup($pidms){
        $this->employeeInfo->setup($pidms);
        $this->studentInfo->setup($pidms);
        $this->emeritiInfo->setup($pidms);
    }



    public function getSubClass($pidm){



        if($this->employeeInfo->isEmployee($pidm)){
            if($this->employeeInfo->isHighSchool($pidm)){
                return 'HSE';
            }

            if($this->employeeInfo->isNonMiamiCollegeStudentEmp($pidm)) {
                return 'NMCSE';
            }
            if($this->studentInfo->isGradAssistant($pidm) && $this->employeeInfo->isGradAssistant($pidm) ){
                return 'GA';
            }
            if($this->employeeInfo->isPayrollDeduct($pidm)) {
                return 'EMP';
            }
            return 'EMP-PIO';

        }



        if($this->studentInfo->isEnrolledFallSpring($pidm)) {

            if ($this->studentInfo->isRegionalStudent($pidm)) {
                if ($this->studentInfo->isFirstYear($pidm) && $this->studentInfo->isHeritageCommonsResident($pidm)) {
                    return "FY-HC";
                }
                if ($this->studentInfo->isPathWayStudent($pidm)) {
                    return "NPTH";
                }

                return 'REG';

            }

            if ($this->studentInfo->isPostSecondaryStudent($pidm)) {
                return 'POST';
            }

            if ($this->studentInfo->isNonDegreeStudent($pidm)) {

                return 'NDS';
            }

            if ($this->studentInfo->isDoctoralStudent($pidm)) {

                return 'DOC';
            }


            if ($this->studentInfo->isGradStudent($pidm)) {
                return 'GS';
            }


            if ($this->studentInfo->isNonTraditionalStudent($pidm)) {

                return 'NON';

            }

            if ($this->studentInfo->isSenior($pidm)) {
                if ($this->studentInfo->isHeritageCommonsResident($pidm)) {
                    return 'SR-HC';
                }
                if ($this->studentInfo->isResidentHallStudent($pidm)) {
                    return 'SR-RH';
                }
                if ($this->studentInfo->isOffCampusStudent($pidm)) {
                    return 'SR-OFF';
                }
                if ($this->studentInfo->isCommuterStudent($pidm)) {
                    return 'SR-OFF';
                }

                return 'SR-OFF';
            }

            if ($this->studentInfo->isJunior($pidm)) {
                if ($this->studentInfo->isHeritageCommonsResident($pidm)) {
                    return 'JR-HC';
                }
                if ($this->studentInfo->isResidentHallStudent($pidm)) {
                    return 'JR-RH';
                }
                if ($this->studentInfo->isOffCampusStudent($pidm)) {
                    return 'JR-OFF';
                }
                if ($this->studentInfo->isCommuterStudent($pidm)) {
                    return 'JR-OFF';
                }

                return 'JR-OFF';
            }

            if ($this->studentInfo->isSophomore($pidm)) {
                if ($this->studentInfo->isHeritageCommonsResident($pidm)) {
                    return 'SOF-HC';
                }
                if ($this->studentInfo->isOffCampusStudent($pidm)) {
                    return 'SOF-OFF';
                }
                if ($this->studentInfo->isCommuterStudent($pidm)) {
                    return 'SOF-RH';
                }

                return 'SOF-RH';
            }
            if ($this->studentInfo->isFirstYear($pidm)) {
                if ($this->studentInfo->isHeritageCommonsResident($pidm)) {
                    return 'FY-HC';
                }
                if ($this->studentInfo->isCommuterStudent($pidm)) {
                    return 'FY-C';
                }
                if ($this->studentInfo->isFirstYearStudentDistance($pidm)) {
                    return 'FY-D';
                }
                return 'FY';
            }

        }

        if($this->emeritiInfo->isRetiree($pidm)) {
            return 'EMER';

        }

        if($this->emeritiInfo->isEmeriti($pidm)) {
            return 'EMER';

        }

        if($this->emeritiInfo->isAlumni($pidm)) {
            return 'EMER';

        }


        return 'V';
    }






}