<?php
/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 9:17 AM
 */

namespace MiamiOH\RestngParking\Services;


class NuparkEmployee extends \MiamiOH\RESTng\Service
{

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    private $employeeInformation =[];


    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    public function setup($pidms){
        if(is_scalar($pidms)){
            $pidms=array($pidms);
        }
        $values = $pidms;
        $queryString='select pebempl.pebempl_pidm, pebempl.pebempl_ecls_code, pebempl.pebempl_bcat_code,pebempl.pebempl_egrp_code
                        from pebempl
                        where pebempl_empl_status <> \'T\'
                        and pebempl_bcat_code <> \'ST\'
                        and pebempl_pidm 
                        in ('.implode(',', array_fill(0, count($values), '?')).")";

       // echo var_dump($queryString);
        $records = $this->dbh->queryall_array($queryString, $values);
        for($i=0;$i<count($records);$i++){
            $this->createEmployeeElementFromRecord($records[$i]);

        }

        return $records;
    }


    public function createEmployeeElementFromRecord($record)
    {

        $model = [];
        $model['pidm'] = $record['pebempl_pidm'];
        $model['ecls_code'] = $record['pebempl_ecls_code'];
        $model['bcat_code'] = $record['pebempl_bcat_code'];
        $model['egrp_code'] = $record['pebempl_egrp_code'];
        $this->employeeInformation[$record['pebempl_pidm']]=$model;

    }

    public function isHighSchool($pidm){

        if(isset($this->employeeInformation[$pidm])){
            if($this->employeeInformation[$pidm]['ecls_code'] == 'HS'){
                return true;
            }
        }

        return false;

    }

    public function isEmployee($pidm){
        if(isset($this->employeeInformation[$pidm])){
            return true;
        }
        return false;
    }


    public function isPayrollDeduct($pidm){
        if(isset($this->employeeInformation[$pidm])){
            $elsc =$this->employeeInformation[$pidm]['ecls_code'] ?? null;
            $egrp = $this->employeeInformation[$pidm]['egrp_code'] ?? null;
            if( $elsc == 'NP' || $elsc == 'SC' || $egrp == 'ERET' ){
                //Don't have Payroll Deduction Available
                return false;
            }
            //Have Payroll Deduction Available
            return true;
        }
        //Not an Employee
        return false;
    }

    public function isGradAssistant($pidm)
    {
        if (isset($this->employeeInformation[$pidm])) {
            if($this->employeeInformation[$pidm]['bcat_code'] == 'GA'){
                return true;
            }
        }
        return false;
    }


    public function isNonMiamiCollegeStudentEmp($pidm)
    {
        if (isset($this->employeeInformation[$pidm])) {
            if($this->employeeInformation[$pidm]['bcat_code'] == 'OU'){
                return true;
            }
        }
        return false;
    }

}