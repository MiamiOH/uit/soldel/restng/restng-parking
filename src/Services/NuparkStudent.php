<?php
/**
 * Created by PhpStorm.
 * User: nanjals
 * Date: 5/22/18
 * Time: 12:35 PM
 */

namespace MiamiOH\RestngParking\Services;


class NuparkStudent extends \MiamiOH\RESTng\Service
{


    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    private $studentInformation = [];


    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }


    public function setup($pidms)
    {

        if (is_scalar($pidms)) {
            $pidms = array($pidms);
        }
        $values = $pidms;
        $queryString = 'With allStudentRecords as (' .
'select sgbstdn.sgbstdn_pidm, sgbstdn.sgbstdn_rate_code, sgbstdn.sgbstdn_levl_code,
sgbstdn.sgbstdn_term_code_eff,sgbstdn.sgbstdn_styp_code,slrrasg_bldg_code,sgbstdn.sgbstdn_camp_code,SUBSTR (goksels.f_address_telephone_value
(sgbstdn_pidm,\'ENRLADDR\',\'A\',SYSDATE,1, \'S\',NULL,\'ZIP\',\'A\',NULL),1,5) as zipcode, 
 TRUNC (MONTHS_BETWEEN (SYSDATE, spbpers_birth_date) / 12) as age,
 f_class_calc_fnc (sgbstdn_pidm, sgbstdn_levl_code, sgbstdn_term_code_eff) as classCode,
 fz_enrolled_this_term( sgbstdn_pidm, fz_get_term(\'N\',\'S\'))
  as fallSpringEnrolled ,
 DECODE(distanceQuery.pidm, NULL, \'N\', \'Y\') as distance  
  from sgbstdn  
  full outer join (select slrrasg_pidm, slrrasg_bldg_code
  from slrrasg 
   where slrrasg_term_code = fz_get_term(\'N\',\'S\') 
    and slrrasg_ascd_code = \'AC\' 
    group by slrrasg_pidm,slrrasg_bldg_code) 
         on sgbstdn_pidm = slrrasg_pidm 
    full outer join spbpers
        on sgbstdn_pidm = spbpers_pidm
        full outer join (SELECT distinct(spraddr_pidm) as pidm
           
           FROM spraddr a, cln_postal_codes src, cln_postal_codes dest
          WHERE     src.postal_code = SUBSTR (\'45056\', 1, 5) -- Stationary Location
    AND src.country_code = \'US\'
    AND SUBSTR (a.spraddr_ZIP, 1, 5) = dest.postal_code
    AND SYSDATE BETWEEN NVL (a.spraddr_from_date, SYSDATE - 1)
    AND NVL (a.spraddr_to_date, SYSDATE + 1)
    AND NVL (LTRIM (UPPER (a.spraddr_status_ind)), \'A\') = \'A\'
    AND dest.country_code = \'US\'
    AND a.spraddr_atyp_code = \'MA\'
    AND ROUND (CLEAN_Address.Get_Distance (
            src.latitude,
            -1 * ABS (src.longitude),
            dest.latitude,
            -1 * ABS (dest.longitude)),
        2) >= 190) distanceQuery
    on sgbstdn_pidm = distanceQuery.pidm
where sgbstdn_term_code_eff = fz_get_term(\'N\',\'S\')' .
            '),'  //end of query  allStudentRecords
. 'pathWayStudents as (' .
            'select SGRSATT_ATTS_CODE, SGRSATT_PIDM from sgrsatt 
where SGRSATT_TERM_CODE_EFF >= fz_get_term(\'N\',\'S\')
    and SGRSATT_ATTS_CODE = \'NPTH\' '
        . ')'  //end of query  pathWayStudents
. 'select * from allStudentRecords ' .
' full outer join pathWayStudents ' .
' on allStudentRecords.sgbstdn_pidm = pathWayStudents.SGRSATT_PIDM' .
' where  sgbstdn_pidm  in (' . implode(',', array_fill(0, count($values), '?')) . ")";

        $records = $this->dbh->queryall_array($queryString, $values);

        $numberOfRecords = count($records);
        for ($i = 0; $i < $numberOfRecords; $i++) {
            $this->createStudentElementFromRecord($records[$i]);

        }


    }


    public function createStudentElementFromRecord($record)
    {

        $model = [];
        $model['pidm'] = $record['sgbstdn_pidm'];
        $model['rate_code'] = $record['sgbstdn_rate_code'];
        $model['pathWay'] = $record['sgrsatt_atts_code'];
        $model['level_code'] = $record['sgbstdn_levl_code'];
        $model['term_code_eff'] = $record['sgbstdn_term_code_eff'];
        $model['styp_code'] = $record['sgbstdn_styp_code'];
        $model['camp_code'] = $record['sgbstdn_camp_code'];
        $model['bldg_code'] = $record['slrrasg_bldg_code'];
        $model['zip_code'] = $record['zipcode'];
        $model['age'] = $record['age'];
        $model['class_code'] = $record['classcode'];
        $model['fall_spring_enrolled'] = $record['fallspringenrolled'];
        $model['distance'] = $record['distance'];
        $this->studentInformation[$record['sgbstdn_pidm']] = $model;

    }


    public function isEnrolledFallSpring($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['fall_spring_enrolled'] == 'Y') {
                return true;
            }
        }

        return false;

    }

    public function isRegionalStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['camp_code'] != 'O') {
                return true;
            }
        }

        return false;

    }

    public function isNonTraditionalStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['age'] > 25) {
                return true;
            }
        }

        return false;

    }

    public function isDoctoralStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '15') {
                return true;
            }
        }

        return false;

    }


    public function isNonDegreeStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '99') {
                return true;
            }
        }

        return false;

    }

    public function isGradStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['level_code'] == 'GR') {
                return true;
            }
        }

        return false;

    }

    public function isSenior($pidm)
    {
        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '04') {
                return true;
            }
        }

        return false;
    }

    public function isJunior($pidm)
    {
        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '03') {
                return true;
            }
        }

        return false;
    }

    public function isSophomore($pidm)
    {
        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '02') {
                return true;
            }
        }

        return false;
    }

    public function isFirstYear($pidm)
    {
        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['class_code'] == '01') {
                return true;
            }
        }

        return false;
    }

    public function isOffCampusStudent($pidm)
    {
        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['zip_code'] == '45056' && $this->studentInformation[$pidm]['bldg_code'] == null) {
                return true;
            }
        }

        return false;
    }


    public function isHeritageCommonsResident($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if (stripos($this->studentInformation[$pidm]['bldg_code'], 'HC') === 0) {
                return true;
            }
        }

        return false;

    }


    public function isResidentHallStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if($this->studentInformation[$pidm]['bldg_code'] && $this->studentInformation[$pidm]['bldg_code'] != null){
                return true;
            }
        }

        return false;

    }


    public function isCommuterStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['zip_code'] != '45056' && $this->studentInformation[$pidm]['bldg_code'] == null) {
                return true;
            }
        }

        return false;

    }

    public function isGradAssistant($pidm)
    {
        if(isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['rate_code'] == 'GAWD' && $this->studentInformation[$pidm]['level_code'] == 'GR') {
                return true;
            }
        }
        return false;
    }

    public function isPostSecondaryStudent($pidm)
    {
        if ($this->studentInformation[$pidm]['styp_code'] == 'A' || $this->studentInformation[$pidm]['styp_code'] == 'B') {
            return true;
        }
        return false;
    }


    public function isFirstYearStudentDistance($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['distance'] == 'Y') {
                return true;
            }
        }

        return false;

    }


    public function isPathWayStudent($pidm)
    {

        if (isset($this->studentInformation[$pidm])) {
            if ($this->studentInformation[$pidm]['pathWay'] == 'NPTH') {
                return true;
            }
        }

        return false;

    }


}