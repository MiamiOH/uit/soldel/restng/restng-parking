# Person RESTng Web Service
This web service provides basic parking information about a specific person. Currently, used by the NuPark integrations 
to determine the type of parking pass a person should be granted. 
## Docker Setup
1. Create the following files in the docker/config folder. Example files are provided as a starting point:
    * authorizations.yaml
        * Provides Authorizations for the Web Service. This basically mimics access granted via CAM. The example is 
          set up to allow the user bob to access the Parking Web Service.
    * credentials.yaml
        * Credentials that can be used to access the locally hosted web service. Token here is especially useful since 
          you can set it to a value that is easy to type, and they never expire. Example provides the set up for a user 
          bob for both a token and a password.
    * datasources.yaml
        * These are database connections to be used by the Data Sources specified by RESTng. You can change the username 
          and environments as needed. 
1. Start up your docker containers.
1. You should be able to now access the web service via the following URL: https://localhost/swagger-ui/